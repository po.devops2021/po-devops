<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/zed.php");

class zedTest extends TestCase
{
    public function test_zed_with_leap_year()
    {
        // Bloc act
        $isLeapYear = zed(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_zed_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = zed(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
