<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/najet.php");

class najeteTest extends TestCase
{
    public function test_najet_with_leap_year()
    {
        // Bloc act
        $isLeapYear = najet(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_najet_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = najet(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
