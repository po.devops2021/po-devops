<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portes ouvertes ETML</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <img src="app/img/etml.jpg" style="width: 100px">
                <h1 style="color: grey; font-size:38px">Portes ouvertes - Informatique - bulle DevOps</h1>
            </div>
        </div>
    </header>

    <main role="main">
        <div class="container">
            <div class="m-3">
                <a href="/app/form.php">Saisir une nouvelle entrée</a>
            </div>
        </div>
        <div class="container">
            <?php
            include_once(__DIR__ . "/app/controler.php");
            CheckFunction();
            echo(DisplayVisitorsList());
            ?>
        </div>
    </main>
    <footer class="text-muted">
        <div class="container">
        </div>
    </footer>

</body>

</html>