<?php

function DbConnection()
{
    $url = getenv("CLEARDB_DATABASE_URL");

    $url = parse_url($url);
    $server = $url["host"];
    $username = $url["user"];
    $password = $url["pass"];
    $db = substr($url["path"], 1);

    $dsn = 'mysql:host=' . $server . ';dbname=' . $db;

    try {
        $dbh = new PDO($dsn, $username, $password, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
        return $dbh;
    } catch (PDOException $e) {
        echo 'Connexion échouée : ' . $e->getMessage();
    }
}

function FetchAll($dbh, $query)
{
    $stmt = $dbh->prepare($query);
    $stmt->execute();
    return $stmt->fetchAll();
}

function InsertVisitor($data)
{
    $dbh = DbConnection();
    $query = "INSERT INTO visitor (visitorFirstName, visitorLastName, studentFirstName, studentLastName, phpFunction, poDate, validated) ";
    $query .= "VALUES (:visitorFirstName, :visitorLastName, :studentFirstName, :studentLastName, :phpFunction, :poDate, :validated)";

    //var_dump($query);
    //var_dump($data);

    $stmt = $dbh->prepare($query);
    $stmt->execute($data);
}

function UpdateVisitor($dbh, $visitor)
{
    $stmt = $dbh->prepare("UPDATE visitor SET validated = 1 where phpFunction = :phpFunction");
    $stmt->bindParam(':phpFunction', $visitor['phpFunction'], PDO::PARAM_STR);
    $stmt->execute();
}
