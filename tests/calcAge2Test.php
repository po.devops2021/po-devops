<?php

include(dirname(__FILE__)."/../src/calcAge2.php");

use PHPUnit\Framework\TestCase;

class calcAge2Test extends TestCase
{
    public function testCalcAge2()
    {

        //Bloc act
        $result = calcAge2("04.01.1992");

        //Bloc assert
        $this->assertEquals($result, 30);
    }
}
