<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/tortue2.php");

class tortue2Test extends TestCase
{
    public function test_tortue_with_leap_year()
    {
        // Bloc act
        $isLeapYear = tortue2(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_tortue_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = tortue2(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
