<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/zero.php");

class zeroTest extends TestCase
{
    public function test_jul_with_leap_year()
    {
        // Bloc act
        $isLeapYear = zero(115);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_jul_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = zero(116);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
