# Guide pour les portes ouvertes

## but de ce document

Le but de ce document est d'aider les élèves de l'ETML à présenter l'activité de la bulle "Sécurité & Devops" aux visiteurs le jour des portes ouvertes de l'ETML.

En effet, chaque visiteur sera accompagné par un élève qui devra expliquer et vulgariser l'activité Devops.

L'élève accompagnateur aura en charge de présenter les concepts de devops, Intégration Continue (CI), Déploiement Continu (CD), tests unitaires, pipeline, etc. ([Voir ce document pour plus d'explications](https://www.atlassian.com/fr/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment#:~:text=D%C3%A9ploiement%20continu%20Le%20d%C3%A9ploiement%20continu%20va%20plus%20loin,emp%C3%AAchera%20le%20d%C3%A9ploiement%20en%20production%20d%27un%20nouveau%20changement.))

Des affiches seront présentes dans la salle pour amener des explications sur chacun des termes ci-dessus.

## Introduction de l'activité proposée

Nous souhaitons initier les visiteurs des Portes ouvertes de l'ETML au DevOps.
Pour ce faire, nous allons proposer à chaque visiteur de déployer une nouvelle version d'une mini-application web.

Cette application sera re-déployée après l'ajout de chaque nouvelle fonctionnalité.

Comme nous n'avons, bien sûr, pas le temps de faire développer une vraie fonctionnalité à chaque visiteur, nous allons lui proposer :

- soit d'ajouter un test unitaire à une fonction php existante.
- soit d'écrire une nouvelle fonction php et un test unitaire associé.

Cette nouvelle fonction (ou ce nouveau test unitaire) représentera la nouvelle fonctionnalité.

Ensuite, le visiteur doit tester en local (c'est à dire sur la machine sur laquelle il est en train de travailler) 2 choses :

- que le code respecte les normes de codage PHP
- que les tests unitaires soient tous exécutés avec succès

Dès que la nouvelle fonctionnalité est terminée et que les tests sont OK, nous souhaitons déployer cette nouvelle fonctionnalité en "Production".

Pour cela, le visiteur doit "pousser" ses modifications dans git.

La dernière action déclenche la phase d'intégration continue (CI) qui se compose de 2 étapes exécutées en parallèle :

- l'exécution des tests unitaires sur une machine montée dans le cloud grâce à docker
- la validation des conventions de codage sur une autre machine également montée dans le cloud grâce à docker

Si tout cela se passe correctement, l'application sera redéployée sur Heroku à cette adresse https://floating-badlands-98608.herokuapp.com/

Cette étape est appelée le déploiement continu.

## Avantages de l'intégration continue :

- Moins de bugs sont livrés en production, car les régressions sont capturées rapidement par les tests automatisés.
- Le développement de la version est simple, car tous les problèmes d'intégration ont été résolus dès le début.
- Réduction du nombre de changements de contexte : les développeurs sont alertés dès qu'ils rencontrent un problème sur le build et peuvent travailler à sa résolution avant de passer à une autre tâche.
- Les coûts des tests sont considérablement réduits : votre serveur de CI peut exécuter des centaines de tests en quelques secondes.
- Votre équipe de QA passe moins de temps à tester et peut se concentrer sur d'importantes améliorations de la culture de la qualité.

([Source](https://www.atlassian.com/fr/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment#:~:text=D%C3%A9ploiement%20continu%20Le%20d%C3%A9ploiement%20continu%20va%20plus%20loin,emp%C3%AAchera%20le%20d%C3%A9ploiement%20en%20production%20d%27un%20nouveau%20changement.))

## Avantages du déploiement continu :

- Vous pouvez développer plus rapidement, car vous n'avez pas besoin d'interrompre le développement pour les livraisons
- Les pipelines de déploiement sont déclenchés automatiquement pour chaque changement
- Les versions sont moins risquées et plus faciles à corriger en cas de problème lorsque vous déployez de petits lots de changements
- Les clients constatent un flux constant d'améliorations et voient la qualité augmenter au quotidien, plutôt que sur une base mensuelle, trimestrielle ou annuelle.

([Source](https://www.atlassian.com/fr/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment#:~:text=D%C3%A9ploiement%20continu%20Le%20d%C3%A9ploiement%20continu%20va%20plus%20loin,emp%C3%AAchera%20le%20d%C3%A9ploiement%20en%20production%20d%27un%20nouveau%20changement.))

## Installation du projet

### Prérequis :

Vous devez avoir installé sur votre machine :

- git
- php
- composer

### Git bash

Nous allons exécuter les commandes ci-dessous dans `git-bash`.

Il faut donc ouvrir `git-bash`.

### Récupérer le code de l'application

Pour récupérer le code du repository de l'application, placez vous dans un nouveau répertoire
de travail et taper la commande suivante :

`git clone https://gitlab.com/po.devops2021/po-devops`

### Installation des dépendances

Il y a un fichier `composer.json` qui contient la liste des dépendances.

Pour installer les dépendances, il suffit de faire un `composer install`.

Attention : il faut être à la racine du gitclone

Cette commande va créer un répertoire `vendor` et va installer les dépendances dans ce répertoire.

### Tester que l'installation est ok

Pour cela, il suffit de lancer la commande suivante dans un `git bash` :

`vendor/bin/phpunit tests/`

<img title="run unit tests" alt="run unit tests" src="images/run-tests.png">

## Guide de la procédure

### Etape 1 : Remplir le formulaire sur l'application

https://floating-badlands-98608.herokuapp.com/app/form.php

Remarques :

Pour le champ "Fonction php", vous devez saisir un nom de fonction :

- qui commence par une minuscule
- qui se termine par .php
- qui respecte la notation camelCase
- qui n'existe pas encore

### Etape 2 : Ajouter le fichier de la fonction et le fichier du test unitaire

A cette étape vous devez :

- soit écrire une fonction php de votre choix ainsi qu'au moins un test unitaire permettant de valider le bon fonctionnement de votre fonction.
- soit prendre une fonction php présente ici : https://github.com/ETML-INF/php-unit-tests

Remarques:

- le nom du fichier contenant la fonction doit commencer par une minuscule. Exemple : maFonction.php
- le nom du fichier contenant le ou les tests unitaires doit avoir le nom suivant : maFonctionTest.php

Attention : bien vérifier que la fonction que vous allez prendre n'existe pas déjà

### Etape 3 : Je vérifie en local que les tests unitaires passent

Il suffit de lancer la commande suivante dans un `git bash` :

`vendor/bin/phpunit tests/`

<img title="run unit tests" alt="run unit tests" src="images/run-unit-tests.png">

### Etape 4 : Je vérifie en local la qualité du code

Il suffit de lancer la commande suivante dans `git bash` :

`vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --diff`

Cette commande indique les lignes php qui ne respectent pas les conventions de codage.
Mais cette commande ne modifie rien.

Si vous voulez que les modifications se fassent automatiquement :

`vendor/bin/php-cs-fixer fix --allow-risky=yes`

### Etape 5 : Je pousse les nouveaux fichiers créés

Il suffit de lancer la commande suivante dans `git bash` pour pousser vos modifications dans gitlab.

`./vendor/bin/php-cs-fixer fix --allow-risky=yes; git pull; git add .; git commit -m "new entry"; git push`

### Etape 6 : Pipeline CI / CD

L'étape 5 a déclenché la phase d'intégration continue (CI) qui se compose de 2 étapes lancées en parallèle :

- l'exécution des tests unitaires sur une machine montée dans le cloud grâce à docker
- la validation des conventions de codage sur une autre machine également montée dans le cloud grâce à docker

Si la phase d'intégration continue est un succès, l'application sera re-déployée sur Heroku à l'adresse suivante:
https://floating-badlands-98608.herokuapp.com/

Cette étape est appelée le déploiement continu (CD).

Vous pouvez voir l'avancement des 2 phases CI et CD https://gitlab.com/po.devops2021/po-devops/-/pipelines

### Etape 7 : Vérifier que vous trouvez bien votre fonction sur la Homepage

Vous pouvez vérifier qu'une nouvelle entrée s'affiche sur la homepage https://floating-badlands-98608.herokuapp.com/


