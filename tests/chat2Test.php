<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/chat2.php");

class chat2Test extends TestCase
{
    public function test_chat_with_leap_year2()
    {
        // Bloc act
        $isLeapYear = chat2(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_chat_with_no_leap_year2()
    {
        // Bloc act
        $isLeapYear = chat2(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
