# Heroku

## Installation du CLI

Après avoir installé le CLI Heroku, il suffit de lancer un terminal Windows et vérifier que la commande `heroku` répond.

## Login

Ensuite on se connecte à heroku.

`heroku login`

## Création d'une application dans heroku

`heroku create`

```
PS C:\Users\Greg\Documents\ETML\workspace-devops\php-unit-tests> heroku create
Creating app... done, ⬢ floating-badlands-98608
https://floating-badlands-98608.herokuapp.com/ | https://git.heroku.com/floating-badlands-98608.git
```

L'application répond ici https://floating-badlands-98608.herokuapp.com/

## Création d'un API token

Pour que gitlab-ci puisse se connecter à Heroku, il faut définir un API token de l'application web définit dans heroku.

Pour générer ce token:
`heroku auth:token`

https://devcenter.heroku.com/articles/authentication#retrieving-the-api-token

Une fois ce token créé, il faut le mettre : 
- dans les settings de heroku dans le site en question: https://dashboard.heroku.com/apps/floating-badlands-98608/settings
- dans les settings de ci/cd de gitlab: https://gitlab.com/GregLeBarbar/po-ci/-/settings/ci_cd voir la sections variables

## Invalidité du token

le 04.11 j'ai du généré un nouveau token ... je ne sais pas exactement pourquoi l'ancien était invalide

Apparemment il faut créer un token longue vie

$ heroku auth:token
 »   Warning: token will expire 12/05/2021
 »   Use heroku authorizations:create to generate a long-term token
589cd892-5ddd-4284-bbe2-81d6ae3bc9a0

Greg@LAPTOP-5335QT0F MINGW64 ~/Documents/ETML/workspace-devops/gitlab/po-ci (main)
$ heroku authorizations:create
Creating OAuth Authorization... done
Client:      <none>
ID:          a55c3e4a-2263-44b7-bbe4-057ef67b1f1b
Description: Long-lived user authorization
Scope:       global
Token:       45c6e33f-8362-4cb6-9399-55152e2b7b21
Updated at:  Fri Nov 05 2021 10:26:17 GMT+0100 (GMT+01:00) (less than a minute ago)