<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/chat.php");

class chatTest extends TestCase
{
    public function test_chat_with_leap_year()
    {
        // Bloc act
        $isLeapYear = chat(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_chat_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = chat(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
