# Guide pour les portes ouvertes

## But de ce document

Ce document a pour but de vous guider dans le déploiement d'une nouvelle version d'une mini-application web.

Cette application a pour but de présenter quelques pratiques DevOps pour les portes ouvertes de l'ETML.

## Introduction de l'activitée proposée

Nous souhaitons initier par la pratique les visiteurs des Portes ouvertes de l'ETML au DevOps.

Pour ce faire, nous allons proposer à chaque visiteur de déployer une nouvelle version d'une mini-application web. 

Comme nous n'avons pas le temps de faire développer une nouvelle fonctionnalité à chaque visiteur, nous allons lui proposer :
- soit d'ajouter un test unitaire à une fonction php existante.
- soit d'écrire une nouvelle fonction php et un test unitaire associé.

Dès que le visiteur aura terminer sa nouvelle fonctionnalité, il va lancer des vérifications sur un serveur dans le cloud.

C'est étape est appelée l'intégration continue

Définition: Intégration continue

L'intégration continue est un ensemble de pratiques utilisées en génie logiciel consistant à vérifier à chaque modification de code source que le résultat des modifications ne produit pas de régression dans l'application développée.

Si les tests sont passés avec succès, l'application sera déployé sur un autre serveur du cloud.

C'est étape est appelée le déploiement continue

Définition: Déploiement continue
...

à compléter


## Installation du projet

### Récupérer le code de l'application

Prérequis : Vous devez avoir installé :
- git
- php
- composer

Pour récupérer le code de l'application, placez vous dans un nouveau répertoire
de travail et taper la commande suivante :

`git clone https://gitlab.com/GregLeBarbar/po-ci`

## Guide de la procédure

### Etape 1 : remplir le formulaire

### Etape 2 : ajouter le fichier de la fonction et le fichier du test unitaire

### Etape 3 : je vérifie en local la qualité du code

`./vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --diff`

### Etape 4 : je vérifie en local que les tests unitaires passent

`./vendor/bin/phpunit tests`

### Etape 5 : Je pousse les nouveaux fichiers créés

`./vendor/bin/php-cs-fixer fix --allow-risky=yes; git pull; git add .; git commit -m "new entry"; git push`

et voila ! Lorsque CI/CD aura fait son travail on verra le résultat sur la homepage

## Exemples de fonctions PHP et tests unitaires

Vous trouverez dans ce repository github, un ensemble de fonctions PHP et tests unitaires associés.
Ces fonctions ont été écrites par des élèves de 2ème Année de l'ETML.

https://github.com/ETML-INF/php-unit-tests
