<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/test.php");

class testTest extends TestCase
{
    public function test_tortue_with_leap_year()
    {
        // Bloc act
        $isLeapYear = test(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_tortue_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = test(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
