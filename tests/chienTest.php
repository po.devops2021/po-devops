<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/chien.php");

class chienTest extends TestCase
{
    public function test_chien_with_leap_year()
    {
        // Bloc act
        $isLeapYear = chien(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_chien_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = chien(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
