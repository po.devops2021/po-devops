<?php

include(dirname(__FILE__)."/../src/katarina.php");

use PHPUnit\Framework\TestCase;

class katarinaTest extends TestCase
{
    public function testKatarina()
    {

        //Bloc act
        $result = katarina("20.01.2002");

        //Bloc assert
        $this->assertEquals($result, 20);
    }
}
