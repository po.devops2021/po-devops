<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/flo.php");

class floTest extends TestCase
{
    public function test_tortue_with_leap_year()
    {
        // Bloc act
        $isLeapYear = flo(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_tortue_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = flo(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
