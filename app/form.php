<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>Portes ouvertes ETML</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="../index.php"><img src="img/etml.jpg" style="width: 100px"></a>
                <h1 style="color: grey; font-size:38px">Portes ouvertes - Informatique - bulle Web-DB</h1>
            </div>
        </div>
    </header>

    <pre>
        <?php
        include_once(__DIR__ . "/db.php");

        const ERROR_REQUIRED = "Veuillez renseigner ce champ";
        const ERROR_LENGTH   = "Le champ doit avoir un nombre de caractères entre 2 et 50";
        const ERROR_END_PHP_FILE = "Le nom du fichier php doit se terminer par .php";
        const ERROR_START_PHP_FILE = "Le nom du fichier php doit commencer par une minuscule";
        const ERROR_PHP_FILE_ALREADY_EXIST = "Il existe déjà un fichier php avec ce nom";

        $errors = [];
        $visitorFirstName = '';
        $visitorLastName  = '';
        $studentFirstName = '';
        $studentLastName  = '';
        $phpFunction      = '';
        $poDate           = '';

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = filter_input_array(INPUT_POST, [
                'visitorFirstName' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'visitorLastName'  => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'studentFirstName' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'studentLastName'  => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'phpFunction'      => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'poDate'           => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
            ]);

            $visitorFirstName = $_POST['visitorFirstName'] ?? '';
            $visitorLastName  = $_POST['visitorLastName'] ?? '';
            $studentFirstName = $_POST['studentFirstName'] ?? '';
            $studentLastName  = $_POST['studentLastName'] ?? '';
            $phpFunction      = $_POST['phpFunction'] ?? '';
            $poDate           = $_POST['poDate'] ?? '';

            if (!$visitorFirstName) {
                $errors['visitorFirstName'] = ERROR_REQUIRED;
            } elseif (mb_strlen($visitorFirstName) < 2 || mb_strlen($visitorFirstName) > 50) {
                $errors['visitorFirstName'] = ERROR_LENGTH;
            }
            if (!$visitorFirstName) {
                $errors['visitorLastName'] = ERROR_REQUIRED;
            } elseif (mb_strlen($visitorLastName) < 2 || mb_strlen($visitorLastName) > 50) {
                $errors['visitorLastName'] = ERROR_LENGTH;
            }
            if (!$visitorFirstName) {
                $errors['studentFirstName'] = ERROR_REQUIRED;
            } elseif (mb_strlen($studentFirstName) < 2 || mb_strlen($studentFirstName) > 50) {
                $errors['studentFirstName'] = ERROR_LENGTH;
            }
            if (!$visitorFirstName) {
                $errors['studentLastName'] = ERROR_REQUIRED;
            } elseif (mb_strlen($studentLastName) < 2 || mb_strlen($studentLastName) > 50) {
                $errors['studentLastName'] = ERROR_LENGTH;
            }
            if (!$phpFunction) {
                $errors['phpFunction'] = ERROR_REQUIRED;
            } elseif (mb_strlen($phpFunction) < 2 || mb_strlen($phpFunction) > 50) {
                $errors['phpFunction'] = ERROR_LENGTH;
            } elseif (!str_ends_with($phpFunction, ".php")) {
                $errors['phpFunction'] = ERROR_END_PHP_FILE;
            } elseif ($phpFunction[0] === strtoupper($phpFunction[0])) {
                $errors['phpFunction'] = ERROR_START_PHP_FILE;
            } else {
                $dbh = DbConnection();
                $visitors = fetchAll($dbh, "SELECT * FROM visitor");
                foreach ($visitors as $visitor) {
                    if (strtoupper($phpFunction) === strtoupper($visitor["phpFunction"])) {
                        $errors['phpFunction'] = ERROR_PHP_FILE_ALREADY_EXIST;
                        break;
                    }
                }
            }

            if (!$poDate) {
                $errors['poDate'] = ERROR_REQUIRED;
            } elseif (mb_strlen($poDate) < 2 || mb_strlen($poDate) > 50) {
                $errors['poDate'] = ERROR_LENGTH;
            }
            if (sizeof($errors) > 0) {
                // var_dump("Les informations saisies comportent des erreurs: ");
                // var_dump($errors);
            } else {
                $_POST['validated'] = 0;
                InsertVisitor($_POST);
                header('Location: /index.php');
            }
        }
        ?>
        </pre>

    <main role="main">
        <div class="container">
            <h2 class="m-3">Saisir les informations pour un nouveau visiteur</h2>
            <form class="m-3" role="form" method="POST" action="form.php">
                <div class="form-group row">
                    <label for="visitorFirstName" class="col-sm-2 col-form-label">Prénom du visiteur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="visitorFirstName" name="visitorFirstName" placeholder="Prénom du visiteur" value=<?= "$visitorFirstName" ?>>
                        <?= array_key_exists("visitorFirstName", $errors) && $errors["visitorFirstName"] ? '<p style="color:red;">' . $errors["visitorFirstName"] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="visitorLastName" class="col-sm-2 col-form-label">Nom du visiteur</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="visitorLastName" name="visitorLastName" placeholder="Nom du visiteur" value=<?= "$visitorLastName" ?>>
                        <?= array_key_exists("visitorLastName", $errors) && $errors["visitorLastName"] ? '<p style="color:red;">' . $errors["visitorLastName"] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="studentFirstName" class="col-sm-2 col-form-label">Prénom de l'étudiant</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="studentFirstName" name="studentFirstName" placeholder="Prénom de l'étudiant" value=<?= "$studentFirstName" ?>>
                        <?= array_key_exists("studentFirstName", $errors) && $errors["studentFirstName"] ? '<p style="color:red;">' . $errors["studentFirstName"] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="studentLastName" class="col-sm-2 col-form-label">Nom de l'étudiant</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="studentLastName" name="studentLastName" placeholder="Nom de l'étudiant" value=<?= "$studentLastName" ?>>
                        <?= array_key_exists("studentLastName", $errors) && $errors["studentLastName"] ? '<p style="color:red;">' . $errors["studentLastName"] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phpFunction" class="col-sm-2 col-form-label">Fonction php</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phpFunction" name="phpFunction" placeholder="Fonction php" value=<?= "$phpFunction" ?>>
                        <?= array_key_exists("phpFunction", $errors) && $errors["phpFunction"] ? '<p style="color:red;">' . $errors["phpFunction"] . '</p>' : '' ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="poDate" class="col-sm-2 col-form-label">Date et Heure</label>
                    <div class="col-sm-10">
                        <input type="datetime-local" class="form-control" id="poDate" name="poDate" placeholder="Date et heure" value=<?= "$poDate" ?>>
                        <?= array_key_exists("poDate", $errors) && $errors["poDate"] ? '<p style="color:red;">' . $errors["poDate"] . '</p>' : '' ?>
                    </div>
                </div>
                <input type="hidden" name="validated" id="validated" value="0">
                <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <!--<button type="submit" value="" name="submit" class="btn btn-primary" style="width: 30%"></button>-->
                        <button class="btn btn-primary" style="width: 30%" type="submit">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </main>
    <footer class="text-muted">
        <div class="container">
        </div>
    </footer>
</body>

</html>