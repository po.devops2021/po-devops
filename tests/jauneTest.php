<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/jaune.php");

class jauneTest extends TestCase
{
    public function test_jaune_with_leap_year()
    {
        // Bloc act
        $isLeapYear = jaune(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_jaune_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = jaune(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
