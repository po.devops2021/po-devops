<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/haricot.php");

class haricotTest extends TestCase
{
    public function test_haricot_with_leap_year()
    {
        // Bloc act
        $isLeapYear = haricot("T");

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_haricot_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = haricot("N");

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
