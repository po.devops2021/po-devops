<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/jourNaissance.php");

class jourNaissanceTest extends TestCase
{
    public function test_haricot_with_leap_year()
    {
        // Bloc act
        $isLeapYear = jourNaissance(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_haricot_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = jourNaissance(2001);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
