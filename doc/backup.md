# php-unit-tests

## Introduction

Ce repo a pour but de rassembler un ensemble de fonctions php basique.
Pour chaque fonction, quelques tests unitaires sont implémentés.

## Comment lancer les tests unitaires ?

```.\vendor\bin\phpunit```


## Les outils

### phpunit

blabla

### Composerrrrrr

Pour gérer les packages php comme phpunit.

### Heroku CLI

Pour l'utiliser, il faut simplement lancer un Terminal windows et faire `heroku`

## Repo github et CI gitlab

Dans gitlab, faire `new repo` puis `Run CI/CD for external repository`

### fichier .gitlab-ci.yml

J'ai fait ce fichier à partir de l'interface graphique de gitlab.
Ce n'est surement pas la meilleure manière car le repo github du coup ne possède pas ce fichier. 

TODO: déplace le fichier .gitlab-ci.yml dans le repo github

### Est ce qu'une PR github peut trigger le lancement de la pipeline gitlab-ci ?

https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html


## Les extensions vscode

PHP Intelephense

Bracket Pair Colorizer

## vscode - Formater le code php

Le code est formaté dès que je sauve mon fichier.
Pour se faire:
- Fichier > Préférences > Paramètres
- Cliquer sur l'icon afficher les paramètres en json
- Ajouter les lignes ci-dessous
```
    "[php]": {
        "editor.defaultFormatter": "bmewburn.vscode-intelephense-client"
    },
```

Vérifier que:
- Fichier > Préférences > Paramètres
- Saisir Format
- le checkbox `Editor format on save` est bien coché

## Serveur web pour le dev

Pas besoin de xampp, uwamp, etc 

Pour lancer un serveur de dev, il suffit de


`php -S localhost:3000`

## DEMO

### Etape 1 : remplir le formulaire

### Etape 2 : ajouter le fichier de la fonction et le fichier du test unitaire

### Etape 3 : je vérifie en local la qualité du code

`./vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --diff`

### Etape 4 : je vérifie en local que les tests unitaires passent

`./vendor/bin/phpunit tests`

### Etape 5 : Je pousse les nouveaux fichiers créés

`./vendor/bin/php-cs-fixer fix --allow-risky=yes; git pull; git add .; git commit -m "new entry"; git push`

et voila ! Lorsque CI/CD aura fait son travail on verra le résultat sur la homepage
