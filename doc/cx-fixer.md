# php-cs-fixer

## Commandes principales

Pour corriger le code automatiquement:

`./vendor/bin/php-cs-fixer fix --allow-risky=yes`

Pour voir les différences qui seraient exécutées mais sans effectuer les corrections:
`./vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --diff`

## Configuration

Voir le fichier `.php-cs-fixer.php`