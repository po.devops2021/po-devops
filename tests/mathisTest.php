<?php

use PHPUnit\Framework\TestCase;

include_once(dirname(__FILE__) . "/../src/mathis.php");

class mathisTest extends TestCase
{
    public function test_leap_with_leap_year()
    {
        // Bloc act
        $isLeapYear = mathis(2000);

        // Bloc assert
        $this->assertTrue($isLeapYear);
    }

    public function test_leap_with_no_leap_year()
    {
        // Bloc act
        $isLeapYear = mathis(2021);

        // Bloc assert
        $this->assertFalse($isLeapYear);
    }
}
