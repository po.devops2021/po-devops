<?php

include_once(__DIR__ . "/db.php");

function CheckFunction()
{
    $dbh = DbConnection();
    $visitors = fetchAll($dbh, "SELECT * FROM visitor");
    foreach ($visitors as $visitor) {
        //echo 'Record contenant : ' . $visitor["phpFunction"] . ' <br/>';
        $fileFunctionsList = glob('src/*');

        //Loop through the array that glob returned.
        foreach ($fileFunctionsList as $fileFunctionName) {
            if ($visitor["phpFunction"] === basename($fileFunctionName)) {
                //echo 'Fichier présent dans le répertoire des fonctions = valeur en Db : ' . basename($fileFunctionName) . ' <br/>';
                $fileUnitTestsList = glob('tests/*');

                //Loop through the array that glob returned.
                foreach ($fileUnitTestsList as $fileUnitTestName) {
                    //echo 'Fichier : ' . $fileUnitTestName . ' <br/>';

                    // Check testunitaire
                    $parts = explode(".", $visitor["phpFunction"]);
                    $unitTestFileName = $parts[0] . "Test." . $parts[1];

                    //echo 'Fichier : ' . $unitTestFileName . ' <br/>';

                    if ($unitTestFileName == basename($fileUnitTestName)) {
                        //echo 'YES update :  <br/>';
                        UpdateVisitor($dbh, $visitor);
                        break;
                    }
                }
            }
        }
    }
}

function DisplayVisitorsList()
{
    $dbh = DbConnection();
    $visitors = fetchAll($dbh, "SELECT * FROM visitor");
    $visitors = array_reverse($visitors);

    $index = count($visitors);
    $result = "";
    foreach ($visitors as $entry) {
        if ($entry["validated"] == 1) {
            $poDate = new DateTime($entry["poDate"]);
            $poDate = $poDate->format('d/m/Y H:i');
            $result .= "<ul class=\"list-group m-3\">";
            $result .= "<li class=\"list-group-item active\">Entrée n°" . $index . "</li>";
            $result .= "<li class=\"list-group-item\"> Visiteur : <strong>" . $entry["visitorFirstName"] . " " . $entry["visitorLastName"] . "</strong> accompagné de l'Elève " . $entry["studentFirstName"] . " " . $entry["studentLastName"] . "</li>";
            //$result .= "<li class=\"list-group-item\"> Elève : <strong>" . $entry["studentFirstName"] . " " . $entry["studentLastName"] . "</strong></li>";
            $result .= "<li class=\"list-group-item\"> Fonction php : <strong>" . $entry["phpFunction"] . "</strong></li>";
            $result .= "<li class=\"list-group-item\"> Heure : <strong>" . $poDate . "</strong></li>";
            //$result .= "<li class=\"list-group-item\"> Entrée validée ? : " . $entry["validated"] . "</li>";
            $result .= "</ul>";
        }
        $index = $index - 1;
    }
    return $result;
}
